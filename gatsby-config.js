module.exports = {
    siteMetadata: {
        title: `Food Service`,
        description: ``,
        author: `Franco Marino`,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-catch-links`,
        'gatsby-plugin-remark-collection',
        {
            resolve: 'gatsby-transformer-remark',
            options: {
                plugins: [
                    {
                        resolve: 'gatsby-remark-images',
                        options: {
                            maxWidth: 620,
                        },
                    },
                ],
            },
        },
        `gatsby-transformer-sharp`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/pages/services`,
                name: 'services',
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/pages/team`,
                name: 'team',
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Food Service`,
                short_name: `FS`,
                start_url: `/`,
                background_color: `#fff`,
                theme_color: `#e73c3c`,
                display: `minimal-ui`,
                icon: `src/images/icon.png`, // This path is relative to the root of the site.
            },
        },
        'gatsby-plugin-offline',
        {
            resolve: `gatsby-plugin-google-fonts`,
            options: {
                fonts: [
                    `Montserrat\:400,700`,
                    `Kaushan Script`,
                    `Droid Serif\:400,700,400,700i`,
                    `Roboto Slab\:400,100,300,700`,
                ],
            },
        },
        {
            resolve: `gatsby-plugin-recaptcha`,
            options: {
                async: true,
                defer: true,
                args: `?onload=callback&render=explicit`,
            },
        },
    ],
}

import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import Navbar from '../components/navbar'
import Header from '../components/Home/header'
import Services from '../components/Home/services'
import About from '../components/Home/about'
import Gallery from '../components/Home/gallery'
import Team from '../components/Home/team'
import Contact from '../components/Home/contact'
import Footer from '../components/footer'
import SEO from '../components/seo'

const IndexPage = ({ data }) => (
    <Layout>
        <SEO title="Home" />
        <Navbar index={true} />
        <Header />
        <About />
        <Services data={data} />
        <Gallery />
        <Team />
        <Contact />
        <Footer />
    </Layout>
)

export const pageQuery = graphql`
    query ServicesIndexQuery {
        allMarkdownRemark(
            filter: { fields: { collection: { eq: "services" } } }
            sort: { fields: frontmatter___index, order: ASC }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        path
                        icon
                        slug
                    }
                }
            }
        }
    }
`

export default IndexPage

import React from 'react'
import Layout from '../components/layout'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
import InfoBar from '../components/infoBar'
import ContactForm from '../components/contactForm'
import SEO from '../components/seo'
import { Container, Row, Col } from 'react-bootstrap'

const Contatti = () => (
    <Layout>
        <SEO title="Contatti" />
        <Navbar index={false} />
        <section id="contact" className="bg-light" name="head">
            <Row className="justify-content-center mb-3">
                <Col className="text-center">
                    <h2 className="section-heading text-uppercase text-dark">
                        Contatti
                    </h2>
                </Col>
            </Row>
            <InfoBar />
            <Row className="justify-content-center mb-5">
                <h3>Oppure scrivici</h3>
            </Row>
            <Container>
                <Row>
                    <Col>
                        <ContactForm />
                    </Col>
                </Row>
            </Container>
        </section>
        <Footer />
    </Layout>
)

export default Contatti

---
path: "/servizi/formazione-risorse-umane"
title: "Formazione Risorse Umane"
slug: "formazione-risorse-umane"
description: "Su richiesta progettiamo percorsi di formazione del personale con l’obiettivo di accrescere e valorizzare le competenze dello staff."
icon: "fa-graduation-cap"
index: 5
---

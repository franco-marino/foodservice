---
path: "/servizi/progettazione-menu"
title: "Progettazione menu"
slug: "progettaziome-menu"
description: "Insieme ai nostri chef, ci occupiamo della progettazione dei menu, costruiti sull’identità del brand, nel rispetto del territorio, attraverso la ricerca di materie prime di qualità, creatività e innovazione."
icon: "fa-book"
index: 6
---

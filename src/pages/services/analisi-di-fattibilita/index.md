---
path: "/servizi/analisi-di-fattibilita"
title: "Analisi di fattibilità"
slug: "analisi-di-fattibilita"
description: "Aiutiamo il cliente nello studio dei costi previsti per il progetto concentrandoci sul food cost e sul labour cost, individuando le figure professionali necessarie in base al concept del format. Offriamo assistenza per la revisione dei punti critici emersi dallo studio di fattibilità e per garantire l’equilibrio economico del progetto."
icon: "fa-line-chart"
index: 1
---

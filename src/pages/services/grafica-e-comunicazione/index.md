---
path: "/servizi/grafica-e-comunicazione"
title: "Grafica e Comunicazione"
description: "Studio e progettazione grafica dell’immagine coordinata dell’azienda (logo, allestimenti, carta stampata, presentazioni, sito web, social network, ecc). Elaborazione del piano di comunicazione per valorizzare gli obiettivi, esprimere la mission e trasmettere in maniera coerente il brand."
icon: "fa-camera-retro"
index: 7
---

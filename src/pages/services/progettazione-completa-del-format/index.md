---
path: "/servizi/progettazione-completa-del-format"
title: "Progettazione completa del format"
slug: "progettazione-completa-del-format"
description: "Realizziamo progetti a 360° partendo dall’ideazione del concept, dall’analisi di geomarketing e dallo studio di fattibilità per la corretta elaborazione del business plan e la valutazione della sostenibilità del progetto. Nella fase progettuale studiamo gli aspetti architettonici, allestitivi e grafici che il format assumerà. Nella fase operativa, assistiamo il cliente nella ricerca, selezione e formazione delle risorse umane e sviluppiamo il piano di comunicazione. Il cliente sarà costantemente seguito nelle prime fasi dell’attività fino a quando questa non sarà totalmente funzionante in maniera autonoma."
icon: "fa-briefcase"
index: 8
---

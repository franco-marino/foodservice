import React from 'react'
import Layout from '../components/layout'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
import SEO from '../components/seo'
import { graphql } from 'gatsby'
import { Container, Row, Col } from 'react-bootstrap'

const Servizi = ({ data }) => {
    let side = 'left'
    return (
        <Layout>
            <SEO title="Servizi" />
            <Navbar index={false} />
            <section className="bg-light" name="head">
                <Container>
                    <Row className="justify-content-center">
                        <Col className="text-center">
                            <h2 className="section-heading text-uppercase">
                                Servizi
                            </h2>
                            <h3 className="section-subheading text-muted">
                                Un elenco di quello che possiamo offrirti
                            </h3>
                        </Col>
                    </Row>
                    {data.allMarkdownRemark.edges.map(work => {
                        if (side === 'left') {
                            side = ''
                            return (
                                <Row
                                    className="text-center mb-5 p-5"
                                    key={work.node.id}
                                    style={{
                                        borderBottom: '2px solid #ededed',
                                    }}
                                    id={work.node.frontmatter.slug}
                                >
                                    <Col
                                        lg={6}
                                        md={6}
                                        sm={6}
                                        className="align-self-center"
                                    >
                                        <i
                                            className={
                                                'fa ' +
                                                work.node.frontmatter.icon +
                                                ' text-danger'
                                            }
                                            style={{ fontSize: '7em' }}
                                        ></i>
                                    </Col>
                                    <Col lg={6} md={6} sm={6}>
                                        <h5 className="text-danger">
                                            {work.node.frontmatter.title}
                                        </h5>
                                        <p className="text-muted">
                                            {work.node.frontmatter.description}
                                        </p>
                                    </Col>
                                </Row>
                            )
                        } else {
                            side = 'left'
                            return (
                                <Row
                                    className="text-center mb-5 p-5 flex-column-reverse flex-md-row"
                                    key={work.node.id}
                                    style={{
                                        borderBottom: '2px solid #ededed',
                                    }}
                                    id={work.node.frontmatter.slug}
                                >
                                    <Col lg={6} md={6} sm={6}>
                                        <h5 className="text-danger">
                                            {work.node.frontmatter.title}
                                        </h5>
                                        <p className="text-muted">
                                            {work.node.frontmatter.description}
                                        </p>
                                    </Col>
                                    <Col
                                        lg={6}
                                        md={6}
                                        sm={6}
                                        className="align-self-center"
                                    >
                                        <i
                                            className={
                                                'fa ' +
                                                work.node.frontmatter.icon +
                                                ' text-danger'
                                            }
                                            style={{ fontSize: '7em' }}
                                        ></i>
                                    </Col>
                                </Row>
                            )
                        }
                    })}
                </Container>
            </section>
            <Footer />
        </Layout>
    )
}

export const pageQuery = graphql`
    query ServiziIndexQuery {
        allMarkdownRemark(
            filter: { fields: { collection: { eq: "services" } } }
            sort: { fields: frontmatter___index, order: ASC }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        title
                        path
                        icon
                        description
                        slug
                    }
                }
            }
        }
    }
`

export default Servizi

---
name: "Sebastiano Graziano"
role: "Restaurant Manager"
image: "/pages/team/sebastiano-graziano/team.jpg"
slug: "sebastiano-graziano"
contacts: [
     {
     "icon": "linkedin", 
     "link": "https://www.linkedin.com/in/sebastiano-graziano-52663282?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BUPlVmNXDSHyQkqFRp3T2sQ%3D%3D",
     "color": "#3562ab"
      }
]
index: 0

---

---
name: "Rita Graziano"
role: "Visual Designer"
image: "/pages/team/rita-graziano/team.jpg"
slug: "rita-graziano"
contacts: [
     {
     "icon": "linkedin", 
     "link": "https://www.linkedin.com/in/rita-graziano-67065320",
     "color": "#4875B4"
      },
      {
      "icon": "facebook",
      "link": "https://www.facebook.com/cultwelcome/",
      "color": "#3b5998"
      }
]
index: 2
---

import React, { useEffect } from 'react'
import { Link } from 'gatsby'
import { Navbar, Nav, Image } from 'react-bootstrap'
import logo from '../images/logo.png'

const navbar = ({ index }) => {
    useEffect(() => {
        let navbar = document.getElementById('mainNav')

        if (!index) navbar.classList.add('navbar-shrink')

        // Scroll Event
        window.addEventListener('scroll', () => {
            // Shrink nav
            if (index) shrinkNav(navbar)
        })
    })

    const shrinkNav = navbar => {
        if (window.pageYOffset > 100) navbar.classList.add('navbar-shrink')
        else navbar.classList.remove('navbar-shrink')
    }

    return (
        <>
            <Navbar
                collapseOnSelect
                variant="dark"
                expand="lg"
                fixed="top"
                id="mainNav"
                name="aaa"
            >
                <Navbar.Brand href="/">
                    <Image src={logo} id="brand-logo" className="d-none" />
                    <span id="brand-title">Food Service</span>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="navbarResponsive">
                    <Nav className="ml-auto text-uppercase">
                        <Nav.Item>
                            <Link to="/" className="nav-link">
                                Home
                            </Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Link to="/servizi" className="nav-link">
                                Servizi
                            </Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Link to="/contatti" className="nav-link">
                                Contatti
                            </Link>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default navbar

import React, { useState } from 'react'
import { Row, Col, Form, FormControl, Button } from 'react-bootstrap'
import Recaptcha from 'react-recaptcha'

const contactForm = () => {
    // Initialize state
    const [isVerified, setVerified] = useState(false)
    const [error, setError] = useState(null)
    const [contactForm, setForm] = useState({
        name: '',
        email: '',
        phone: '',
        message: '',
    })

    const { name, email, phone, message } = contactForm

    const callback = () => {
        console.log('Loaded')
    }
    const verify = response => {
        if (response) setVerified(true)
    }
    const onchange = e => {
        setForm({ ...contactForm, [e.target.name]: e.target.value })
    }
    const sendEmail = e => {
        e.preventDefault()
        // Check field
        if (name === '' || email === '' || phone === '' || message === '') {
            setError('Errore: compila tutti i campi')
        } else if (!isVerified) {
            setError('Errore: Recaptcha mancante')
        } else {
            setForm({
                ...contactForm,
                name: '',
                email: '',
                phone: '',
                message: '',
            })
            setError(false)
            e.target.submit()
        }
    }
    return (
        <>
            <Form
                name="contact"
                method="POST"
                netlify-honeypot="bot-field"
                data-netlify="true"
                data-netlify-recaptcha="true"
                onSubmit={sendEmail}
            >
                <input type="hidden" name="contact" value="contact" />
                <p className="d-none">
                    <label>
                        Don't fill this out if you're human:{' '}
                        <input name="bot-field" />
                    </label>
                </p>
                <Row>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Control
                                type="text"
                                placeholder="Nome *"
                                name="name"
                                value={name}
                                onChange={onchange}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Control
                                type="email"
                                placeholder="Email *"
                                name="email"
                                value={email}
                                onChange={onchange}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Control
                                type="tel"
                                placeholder="Telefono *"
                                name="phone"
                                value={phone}
                                onChange={onchange}
                            />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <FormControl
                                as="textarea"
                                placeholder="Messaggio *"
                                name="message"
                                value={message}
                                onChange={onchange}
                            />
                        </Form.Group>
                    </Col>
                    <Col md={12}>
                        <Form.Group>
                            <div data-netlify-recaptcha="true"></div>
                            <Recaptcha
                                sitekey="6Leyp7cUAAAAAB9QFQyZ-XJvITwU57BhSIS643iJ"
                                render="explicit"
                                verifyCallback={verify}
                                onloadCallback={callback}
                            />
                            {error && <p className="text-danger">{error}</p>}
                        </Form.Group>
                    </Col>
                    <div className="clearfix"></div>
                    <Col lg={12} className="text-center">
                        <Button
                            type="submit"
                            variant="primary"
                            size="xl"
                            className="text-uppercase"
                        >
                            invia messaggio
                        </Button>
                    </Col>
                </Row>
            </Form>
        </>
    )
}

export default contactForm

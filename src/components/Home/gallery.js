import React from 'react'
import { Container, Row, Col, Image } from 'react-bootstrap'
const importAll = r => r.keys().map(r)

const gallery = () => {
    const images = importAll(
        require.context('../../images/gallery', false, /\.(png|jpe?g|svg)$/)
    )
    const content = images.map((img, idx) => (
        <Col sm={12} md={4} lg={3} key={idx} className="mb-2">
            <Image fluid src={img} />
        </Col>
    ))

    return (
        <section className="bg-dark">
            <Container>
                <Row className="justify-content-center">
                    <Col className="text-center">
                        <h2 className="section-heading text-uppercase text-white">
                            Gallery
                        </h2>
                        <h3 className="section-subheading text-muted">
                            Alcuni dei nostri precedenti lavori
                        </h3>
                    </Col>
                </Row>
                <Row>{content}</Row>
            </Container>
        </section>
    )
}

export default gallery

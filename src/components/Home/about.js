import React from 'react'
import { Row, Col, Image, Container } from 'react-bootstrap'
import { FaStar, FaHeart, FaSmile, FaBullseye } from 'react-icons/fa'
const experience = require('../../images/about/experience.png')
const customer_satisfaction = require('../../images/about/customer_satisfaction.png')
const accuracy = require('../../images/about/accuracy.png')
const passion = require('../../images/about/passion.png')

const about = () => (
    <section className="bg-dark" name="about">
        <Container>
            <Row>
                <Col lg={12} className="text-center">
                    <h2 className="section-heading text-uppercase text-white">
                        chi siamo
                    </h2>
                    <h3 className="section-subheading text-muted text-justify">
                        Food Service è una società di consulenza che offre
                        un’ampia gamma di servizi personalizzati alle imprese
                        del settore Food & Beverage in fase di start up, restart
                        o restyling.
                        <br />
                        <br />
                        La consulenza offerta ha come principale obiettivo
                        quello di pianificare azioni concrete e facilmente
                        fruibili grazie all’offerta di un servizio accurato
                        specifico per ogni esigenza.
                    </h3>
                </Col>
            </Row>
            <Row className="justify-content-center text-white text-uppercase mb-5">
                <h3 className="h3">i nostri punti di forza</h3>
            </Row>
            <Row className="align-items-center mb-5 text-white">
                <Col lg={6} className="order-2 order-lg-1 text-center">
                    <FaStar
                        size={75}
                        className="mb-3 text-warning d-none d-lg-inline"
                    />
                    <h3 className="text-uppercase">esperienza</h3>
                </Col>
                <Col lg={5} className="px-5 mx-auto order-1 order-lg-2">
                    <Image fluid className="mb-4 mb-lg-0" src={experience} />
                </Col>
            </Row>
            <Row className="align-items-center text-white">
                <Col lg={5} className="px-5 mx-auto">
                    <Image fluid className="mb-4 mb-lg-0" src={passion} />
                </Col>
                <Col lg={6} className="text-center">
                    <FaHeart
                        size={75}
                        className="mb-3 text-primary d-none d-lg-inline"
                    />
                    <h3 className="text-uppercase">passione</h3>
                </Col>
            </Row>
            <Row className="align-items-center mb-5 text-white">
                <Col lg={6} className="order-2 order-lg-1 text-center">
                    <FaBullseye
                        size={75}
                        className="mb-3 text-white d-none d-lg-inline"
                    />
                    <h3 className="text-uppercase">accuratezza</h3>
                </Col>
                <Col lg={5} className="px-5 mx-auto order-1 order-lg-2">
                    <Image fluid className="mb-4 mb-lg-0" src={accuracy} />
                </Col>
            </Row>
            <Row className="align-items-center text-white">
                <Col lg={5} className="px-5 mx-auto">
                    <Image
                        fluid
                        className="mb-4 mb-lg-0"
                        src={customer_satisfaction}
                    />
                </Col>
                <Col lg={6} className="text-center">
                    <FaSmile
                        size={75}
                        className="mb-3 text-success d-none d-lg-inline"
                    />
                    <h3 className="text-uppercase">soddisfazione cliente</h3>
                </Col>
            </Row>
        </Container>
    </section>
)

export default about

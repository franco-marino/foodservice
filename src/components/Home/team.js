import React from "react"
import { Container, Row, Col, Image } from "react-bootstrap"
import { graphql, StaticQuery } from "gatsby"
import { FaFacebookSquare, FaGithub, FaLinkedin } from "react-icons/fa"

export default () => (
    <StaticQuery
        query={graphql`
            query HeadingQuery {
                allMarkdownRemark(
                    filter: { fields: { collection: { eq: "team" } } }
                    sort: { fields: frontmatter___index, order: ASC }
                ) {
                    edges {
                        node {
                            id
                            frontmatter {
                                name
                                role
                                slug
                                contacts {
                                    icon
                                    link
                                    color
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={data => (
            <section id="team" className="bg-light">
                <Container>
                    <Row>
                        <Col lg={12} className="text-center">
                            <h2 className="section-heading text-uppercase">
                                Il nostro team
                            </h2>
                            <h3 className="section-subheading text-muted">
                                Scopri i protagonisti del successo della nostra
                                azienda
                            </h3>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        {data.allMarkdownRemark.edges.map(team => {
                            const image = require(`../../pages/team/${team.node.frontmatter.slug}/team.jpg`)
                            return (
                                <Col
                                    xs={12}
                                    sm={12}
                                    md={12}
                                    lg={6}
                                    xl={6}
                                    key={team.node.id}
                                    className="team-member"
                                >
                                    <Row className="mr-1 ml-1">
                                        <Col>
                                            <Image src={image} roundedCircle />
                                        </Col>
                                        <Col
                                            sm={12}
                                            md={12}
                                            lg={12}
                                            xl={6}
                                            className="my-auto"
                                        >
                                            <h4>
                                                {team.node.frontmatter.name}
                                            </h4>
                                            <p className="text-muted">
                                                {team.node.frontmatter.role}
                                            </p>
                                            <div
                                                className="icon-container"
                                                style={{ minHeight: "31px" }}
                                            >
                                                {team.node.frontmatter.contacts.map(
                                                    (c, i) => (
                                                        <a
                                                            target="a_blank"
                                                            title={c.icon}
                                                            href={c.link}
                                                            style={{
                                                                color: c.color,
                                                            }}
                                                            key={i}
                                                        >
                                                            {c.icon ===
                                                                "github" && (
                                                                <FaGithub
                                                                    size={25}
                                                                />
                                                            )}
                                                            {c.icon ===
                                                                "facebook" && (
                                                                <FaFacebookSquare
                                                                    size={25}
                                                                />
                                                            )}
                                                            {c.icon ===
                                                                "linkedin" && (
                                                                <FaLinkedin
                                                                    size={25}
                                                                />
                                                            )}
                                                        </a>
                                                    )
                                                )}
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            )
                        })}
                    </Row>
                    <Row>
                        <Col lg={8} className="text-center mx-auto">
                            <p className="large text-muted text-left">
                                Un gruppo di professionisti nel quale ognuno
                                potrà offrirvi competenze specifiche maturate
                                nelle diverse esperienze. <br /><br/>
                                Abbiamo lavorato per grandi Catene alberghiere come <b>Carlson Rezidor
                                Hotel</b>, grandi Store come la <b>Rinascente</b> e grandi
                                Famiglie imprenditoriali come la <b>Famiglia
                                Moratti.</b><br/> <br/>
                                Competenze unite alla passione
                                garantiscono un successo assicurato.
                            </p>
                        </Col>
                    </Row>
                </Container>
            </section>
        )}
    />
)

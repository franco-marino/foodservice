import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-scroll'

const Header = () => (
    <>
        <header className="masthead" name="head">
            <div className="intro-text">
                <div className="intro-lead-in">Benvenuto su</div>
                <div className="intro-heading text-uppercase">food service</div>
                <Link
                    activeClass="active"
                    to="about"
                    spy={true}
                    smooth={true}
                    offset={50}
                    duration={500}
                >
                    <Button
                        variant="light"
                        className="text-uppercase text-primary"
                        size="lg"
                    >
                        scopri di più
                    </Button>
                </Link>
            </div>
        </header>
    </>
)

export default Header

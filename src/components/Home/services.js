import React from "react"
import { Container, Row, Col, Button, Card } from "react-bootstrap"
import { Link } from "gatsby"
const services = props => {
    return (
        <>
            <section id="services" className="bg-light">
                <Container>
                    <Row>
                        <Col className="text-center">
                            <h2 className="section-heading text-uppercase">
                                Servizi
                            </h2>
                            <h3 className="section-subheading text-muted text-justify">
                            <b>FOOD SERVICE</b> affianca i propri Clienti del settore Food, Beverage & Hospitality nelle scelte strategiche dallo studio iniziale fino all’avvio di start-up o
                            restyling dell’attività. Obiettivo principale della consulenza di Food Service è il raggiungimento degli obiettivi aziendali in un’ottica di crescita, 
                            la creazione di valore aggiunto per contribuire all’acquisizione di un vantaggio competitivo, fondamentale per distinguersi dalla concorrenza.<br/> 
                            FOOD SERVICE è in grado di seguire in maniera costante e capillare i vari aspetti che coinvolgono il management, lo staff, i fornitori, i clienti, la location,
                            l’immagine e la comunicazione dei vari punti vendita. 
                            </h3>
                        </Col>
                    </Row>
                    <Row className="text-center">
                        {props.data.allMarkdownRemark.edges.map(work => (
                            <Col
                                lg={4}
                                md={6}
                                sm={12}
                                className="mb-3"
                                key={work.node.id}
                            >
                                <Card>
                                    <Card.Body>
                                        <Card.Title>
                                            <i
                                                className={
                                                    "fa " +
                                                    work.node.frontmatter.icon +
                                                    " fa-2x text-danger"
                                                }
                                            ></i>
                                        </Card.Title>
                                        <Card.Text className="font-weight-bold">
                                            <Link
                                                to={
                                                    "/servizi#" +
                                                    work.node.frontmatter.slug
                                                }
                                                style={{
                                                    textDecoration: "none",
                                                    color: "#333",
                                                    fontSize: "1.1rem",
                                                }}
                                            >
                                                {work.node.frontmatter.title}
                                            </Link>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                    <Row className="mt-3 justify-content-center">
                        <Link to="/servizi">
                            <Button
                                size="xl"
                                className="text-uppercase"
                                style={{ cursor: "pointer" }}
                            >
                                scopri di più
                            </Button>
                        </Link>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default services

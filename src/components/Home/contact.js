import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import ContactForm from "../contactForm"
const contact = () => (
    <section id="contact">
        <Container>
            <Row>
                <Col lg={12} className="text-center">
                    <h2 className="section-heading text-uppercase">
                        Contattaci
                    </h2>
                    <h3 className="section-subheading text-muted">
                        Scrivi una mail a{" "}
                        <em style={{ color: "#f00" }}>
                            info@consulenzafood.com
                        </em>{" "}
                        per collaborazioni,info o preventivi.
                    </h3>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <ContactForm />
                </Col>
            </Row>
        </Container>
    </section>
)

export default contact

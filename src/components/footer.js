import React from 'react'
import { Container, Row, Col, Image } from 'react-bootstrap'
import { FaInstagram, FaFacebookF, FaEnvelope, FaGithub } from 'react-icons/fa'
import { Link } from 'react-scroll'
const logo = require('../images/logo.png')

const footer = () => {
    return (
        <footer>
            <Container fluid={true}>
                <Row>
                    <Col md={3}>
                        <Link
                            activeClass="active"
                            to="head"
                            spy={true}
                            smooth={true}
                            offset={50}
                            duration={500}
                        >
                            <Image src={logo} />
                        </Link>
                    </Col>
                    <Col md={3}>
                        <span className="copyright">
                            Copyright &copy; Food Service s.r.l. 2019
                        </span>
                        <br />
                        <span className="text-muted">
                            +39 347 0614760
                            <br />
                            Milano / Padova
                        </span>
                        <br />
                        <a
                            href="//www.iubenda.com/privacy-policy/26865366"
                            className="iubenda-white iubenda-embed"
                            title="Privacy Policy"
                        >
                            Privacy Policy
                        </a>
                    </Col>
                    <Col md={3}>
                        <span className="copyright">
                            Seguici sulle nostre pagine
                        </span>
                        <br />
                        <br />
                        <ul className="list-inline social-buttons">
                            <li className="list-inline-item">
                                <a
                                    target="a_blank"
                                    title="Visita la nostra pagina Facebook"
                                    href="https://fb.me/consulenzafood"
                                >
                                    <FaFacebookF />
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a
                                    target="a_blank"
                                    title="Visita il nostro profilo Instagram"
                                    href="https://www.instagram.com/foodserviceconsulting/?hl=it"
                                >
                                    <FaInstagram />
                                </a>
                            </li>
                        </ul>
                    </Col>
                    <Col md={3}>
                        <span className="copyright">
                            Sito a cura di <b>Franco Marino</b>
                        </span>
                        <br />
                        <br />
                        <ul className="list-inline social-buttons">
                            <li className="list-inline-item">
                                <a
                                    target="a_blank"
                                    title="Scrivimi all'indirizzo franco_marino@outlook.it"
                                    href="mailto:franco_marino@outlook.it"
                                >
                                    <FaEnvelope />
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a
                                    target="a_blank"
                                    title="Seguimi su Github"
                                    href="https://www.github.com/franco-marino"
                                >
                                    <FaGithub />
                                </a>
                            </li>
                        </ul>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default footer

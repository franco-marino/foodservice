import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import {
    FaMapMarkedAlt,
    FaPhone,
    FaFacebookF,
    FaInstagram,
} from 'react-icons/fa'

const infoBar = () => {
    return (
        <Container fluid>
        <Row className="text-center mb-5">
            <Col  className="border border-dark text-dark p-3 m-1">
                <FaMapMarkedAlt size={35} className="mb-2" />
                <p>Milano / Padova</p>
            </Col>
            <Col  className="border border-success text-success p-3 m-1">
                <FaPhone size={35} className="mb-2" /> 
                <p>
                    <a className="text-success" href="tel:3470614760">
                        +39 347 0614760
                    </a>
                </p>
            </Col>
            <Col  className="p-3 m-1" style={{color: "#4267B2",border: "1px solid #4267B2"}}>
                <FaFacebookF size={35} className="mb-2" />
                <p>
                    <a
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://facebook.com/consulenzafood"
                        style={{color: "#4267B2"}}
                    >
                        Visita la pagina
                    </a>
                </p>
            </Col>
            <Col  className="border border-danger text-danger m-1 p-3">
                <FaInstagram size={35} className="mb-2" />
                <p>
                    <a
                        target="_blank"
                        rel="noopener noreferrer"secondary
                        href="https://instagram.csecondaryviceconsulting"
                    >
                        Visita il profilo
                    </a>
                </p>
            </Col>
        </Row>
        </Container>
    )
}

export default infoBar
